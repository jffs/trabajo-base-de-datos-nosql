package com.example.lidimoviles.couchbaseliteapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.couchbase.lite.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {



    DatabaseConfiguration config;
    Database database;
    TextView result, documentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.result);
        documentos = findViewById(R.id.documentos);
        // Get the database (and create it if it doesn’t exist).
        config = new DatabaseConfiguration(getApplicationContext());
        database = null;
    }

    /* Método que carga los datos de un archivo JSON en una base de datos NoSQL del tipo documento/clave-valor */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    public void loadData(View view) {
        try {
            /* Se crea la base de datos, si ya existe, la abre*/
            database = new Database("mydb", config);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        String json = null;
        try {
            // Obtengo los valores que están en el archivo JSON (en la clave 'data'), son los que se van a guardar en la BD
            JSONObject obj = new JSONObject(readJSONFromAsset());
            JSONArray value = (JSONArray) obj.get("data");
            Log.i("Tuplas", String.valueOf(value.length()));
            /* Comienzo a tomar el tiempo para el benchmark de carga de datos */
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < value.length(); i++) {
                // obtengo cada uno de los objetos JSON que estan en 'data' y los guardo
                JSONObject data = value.getJSONObject(i);
                Iterator<String> iter2 = data.keys();
                /* creo un MutableDocument con ID igual a i */
                MutableDocument mutableDoc = new MutableDocument(String.valueOf(i));
                while (iter2.hasNext()) {
                    String key = iter2.next();
                    // creo un nuevo docuento y almaceno los datos del registro
                    mutableDoc.setString(key, String.valueOf(data.get(key)));
                }
                try {
                    database.save(mutableDoc);
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }
            }
            // termino de almacenar todos los datos del archivo. Termina el benchamrk de carga de datos
            long endTime = System.currentTimeMillis();
            String text = "Tiempo total de ejecución carga: " + (endTime - startTime) + "ms";
            int size = getDocumentsCount();
            String text2 = "Documentos guardados " + String.valueOf(size);
            result.setText(text);
            documentos.setText(text2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* Método que devuelve la cantidad de documentos almacenados en la base de datos*/
    private int getDocumentsCount() {
        Query query = QueryBuilder.select(SelectResult.all())
                .from(DataSource.database(database));
        ResultSet queryResult = null;
        try {
            queryResult = query.execute();
            return queryResult.allResults().size();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* Método que actualiza todos los documentos de la base de datos */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    /*el tiempo calculado se toma a partir de tener los documentos en memoria*/
    public void update(View view) {
        MutableDocument mutableDoc;
        int size = getDocumentsCount();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            mutableDoc = database.getDocument(String.valueOf(i)).toMutable();
            mutableDoc.setString("updated_at", String.valueOf(new Date()));
            try {
                database.save(mutableDoc);
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }
        long endTime = System.currentTimeMillis();
        String text = "Tiempo total de ejecución actualización: " + (endTime - startTime) + "ms";
        result.setText(text);
        size = getDocumentsCount();
        text = "Documentos guardados " + String.valueOf(size);
        documentos.setText(text);
    }

    /* Levanto el archivo JSON que está en la carpeta raw, en memoria */
    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getResources().openRawResource(R.raw.smalldataset);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /* Método que elimina la base datos */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    public void dropDB(View view) {
        try {
            database.delete();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    /* Método que ejecuta una query. Devuelve los documentos que tengan la clave-valor 'pais_id'-2*/
    /* muestra el resultado en ms y la cantidad de documentos encontrados con ese criterio*/
    public void executeQuery(View view){
        long startTime = System.currentTimeMillis();
        Query query = QueryBuilder.select(SelectResult.all())
                .from(DataSource.database(database))
                .where(Expression.property("idioma_id").equalTo(Expression.string("5")));
        ResultSet queryResult = null;
        try {
            queryResult = query.execute();
            long endTime = System.currentTimeMillis();
            String text = "Tiempo total de ejecución actualización: " + (endTime - startTime) + "ms";
            result.setText(text);
            text = "Documentos recuperados " + String.valueOf(queryResult.allResults().size());
            documentos.setText(text);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }
}
