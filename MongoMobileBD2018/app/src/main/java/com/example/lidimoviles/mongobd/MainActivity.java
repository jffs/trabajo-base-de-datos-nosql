package com.example.lidimoviles.mongobd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
// Base Stitch Packages
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mongodb.Block;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.lang.NonNull;
import com.mongodb.stitch.android.core.Stitch;
import com.mongodb.stitch.android.core.StitchAppClient;

// Packages needed to interact with MongoDB and Stitch
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;

// Necessary component for working with MongoDB Mobile
import com.mongodb.stitch.android.services.mongodb.local.LocalMongoDbService;
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoCollection;
import com.mongodb.stitch.core.services.mongodb.remote.RemoteUpdateResult;

import org.bson.BsonString;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;

public class MainActivity extends AppCompatActivity {

    TextView result, documentos;
    MongoCollection<Document> localCollection;
    MongoClient mobileClient;
    MongoDatabase database;
    StitchAppClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.result);
        documentos = findViewById(R.id.documentos);
    }



    /* Levanto el archivo JSON que está en la carpeta raw, en memoria */
    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getResources().openRawResource(R.raw.bigdataset); // smalldataset, bigdataset, mediumdataset
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /* Método que carga los datos de un archivo JSON en una base de datos NoSQL del tipo documento/clave-valor */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    public void loadData(View view) {
        // Create the default Stitch Client
        client = Stitch.initializeDefaultAppClient("com.mongodb.app");
        // Create a Client for MongoDB Mobile (initializing MongoDB Mobile)
        mobileClient = client.getServiceClient(LocalMongoDbService.clientFactory);
        // Point to the target collection and insert a document
        database = mobileClient.getDatabase("my_db");
        localCollection =
                database.getCollection("my_collection");
        String json = null;
        try {
            // Obtengo los valores que están en el archivo JSON (en la clave 'data'), son los que se van a guardar en la BD
            JSONObject obj = new JSONObject(readJSONFromAsset());
            JSONArray value = (JSONArray) obj.get("data");
            Log.i("Tuplas", String.valueOf(value.length()));
            /* Comienzo a tomar el tiempo para el benchmark de carga de datos */
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < value.length(); i++) {
                // obtengo cada uno de los objetos JSON que estan en 'data' y los guardo
                JSONObject data = value.getJSONObject(i);
                Iterator<String> iter2 = data.keys();
                /* creo un Document para guardar elementos */
                Document newItem = new Document();
                while (iter2.hasNext()) {
                    String key = iter2.next();
                    // creo un nuevo docuento y almaceno los datos del registro
                    newItem.append(key, String.valueOf(data.get(key)));
                }
                localCollection.insertOne(newItem);
            }
            // termino de almacenar todos los datos del archivo. Termina el benchamrk de carga de datos
            long endTime = System.currentTimeMillis();
            String text = "Tiempo total de ejecución carga: " + (endTime - startTime) + "ms";
            // int size = getDocumentsCount();
            String text2 = "Documentos guardados " + String.valueOf(localCollection.countDocuments());
            result.setText(text);
            documentos.setText(text2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* Método que actualiza todos los documentos de la base de datos */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    /*el tiempo calculado se toma a partir de tener los documentos en memoria*/
    public void update(View view) {
        long startTime = System.currentTimeMillis();

        localCollection.updateMany(new Document(),
                combine(currentDate("lastModified")));

            long endTime = System.currentTimeMillis();
            String text = "Tiempo total de ejecución actualización: " + (endTime - startTime) + "ms";
                    result.setText(text);
            text = "Documentos actualizados " + String.valueOf(localCollection.countDocuments());
                    documentos.setText(text);

    }


    /* Método que ejecuta una query. Devuelve los documentos que tengan la clave-valor 'pais_id'-2*/
    /* muestra el resultado en ms y la cantidad de documentos encontrados con ese criterio*/
    public void executeQuery(View view){
        long startTime = System.currentTimeMillis();
        Document query = new Document();
        query.put("idioma_id", new BsonString("5"));
        FindIterable<Document> cursor = localCollection.find(query);
        ArrayList<Document> results =
                (ArrayList<Document>) cursor.into(new ArrayList<Document>());
            long endTime = System.currentTimeMillis();
            String text = "Tiempo total de ejecución actualización: " + (endTime - startTime) + "ms";
            result.setText(text);
            text = "Documentos recuperados " + String.valueOf(results.size());
            documentos.setText(text);
    }
    /* Método que elimina la base datos */
    /* El método se dispara desde la interfaz cuando se hace tap en el botón correspondiente*/
    public void dropDB(View view) {
        database.drop();
        String text = "Base de datos eliminada";
        result.setText(text);
        text = "";
        documentos.setText(text);
    }
}
